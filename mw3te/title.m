//
//  title.m
//  mw3te
//
//  Created by Carlos Rivero on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "title.h"

@implementation title
@synthesize description = _description;
@synthesize page = _page;
@synthesize type = _type;
@synthesize image = _image;
@synthesize title = _title;



- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    [self setValuesForKeysWithDictionary:dictionary];
//    NSLog(@"The title description is %@",self.description);
//    NSLog(@"The title Image: %@",self.image);
    
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"image"]) {
        UIImage *img = [UIImage imageNamed:value];   
        value = img;
    }
    
    [super setValue:value forKey:key];
    
}

@end
