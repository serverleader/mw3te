//
//  emblems.h
//  mw3te
//
//  Created by Carlos Rivero on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface emblem : NSObject


@property (retain) NSString *description;
@property (retain) NSString *sectionx;
@property (assign) NSString *page;
@property (assign) NSString *type;
@property (strong) UIImage *image;


- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
