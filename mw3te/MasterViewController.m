//
//  MasterViewController.m
//  mw3te
//
//  Created by Carlos Rivero on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "emblem.h"
#import "title.h"
@implementation MasterViewController


@synthesize titles = _titles;
@synthesize emblems = _emblems;
@synthesize detailViewController = _detailViewController;
@synthesize filteredListItems = _filteredListItems;
@synthesize tableView = _tableView;
@synthesize tableViewEmblem = _tableViewEmblem;
@synthesize searchRowSelected = _searchRowSelected;
@synthesize savedSearchTerm;


- (NSMutableArray *)getEmblems
{
    if (_emblems == nil) // so we don't have to read the file every time
    {
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"Emblems"  ofType:@"plist"];
        //NSLog(@">>> path: %@ <<<",sourcePath);
        NSArray *emblemDicts = [NSArray arrayWithContentsOfFile:sourcePath];
        
        if (emblemDicts != nil) {
            
            _emblems = [[NSMutableArray alloc] initWithCapacity:emblemDicts.count];
            
            // Fast enumeration //
            for (NSDictionary *currDict in emblemDicts)
            {
                emblem *emblem1 = [[emblem alloc] initWithDictionary:currDict];
                [_emblems addObject:emblem1];
            }
        }
    }
    
    return _emblems;
}

- (NSMutableArray *)getTitles
{
    if (_titles == nil) // so we don't have to read the file every time
    {
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"Titles"  ofType:@"plist"];
     
        NSArray *titleDicts = [NSArray arrayWithContentsOfFile:sourcePath];
        
        if (titleDicts != nil) {
            
            _titles = [[NSMutableArray alloc] initWithCapacity:titleDicts.count];
            
            // Fast enumeration //
            for (NSDictionary *currDict in titleDicts)
            {
                title *title1 = [[title alloc] initWithDictionary:currDict];
                [_titles addObject:title1];
            }
        }
    }
    
    return _titles;
}



- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
   // NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        
    }else   {
        self.detailViewController = (DetailViewController *)[self.splitViewController.viewControllers lastObject] ;
    }
    
    
    
        
    self.emblems = self.getEmblems;
    self.titles = self.getTitles;    
    
   
    
  
//    if ([self.title isEqualToString:@"Emblems"]) {
//        self.emblems = self.getEmblems;
//        
//        NSLog(@">>> Getting emblems:count %d <<<",self.emblems.count);
//    }else   {
//    
//           self.titles = self.getTitles;
//    }
//    
    
    // create a filtered list that will contain products for the search results table.
    //filteredListItems = [NSMutableArray arrayWithCapacity:[listItems count]];
    self.filteredListItems = [[NSMutableArray alloc] initWithCapacity:[self.titles count]];
    
    // restore search settings if they were saved in didReceiveMemoryWarning.
    // Restore search term
	if ([self savedSearchTerm])
	{
        [[[self searchDisplayController] searchBar] setText:[self savedSearchTerm]];
    }   
    //[self.tableView reloadData];
   // NSLog(@">>> Leaving %s <<<", __PRETTY_FUNCTION__);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
    if (tableView == self.searchDisplayController.searchResultsTableView){
        //NSLog(@"numberOfRows: Filtered Items Count=%i",[self.filteredListItems count]);
        //NSLog(@"Filtered Items Count= %i",[self.achivos count]); 
        // return [self.achivos count];
       // NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
        return [self.filteredListItems count];         
    }
    else{
        if ([self.title isEqualToString:@"Emblems"]) {
         //     NSLog(@"--- Emblems Count: %i ---", [self.emblems count]);
            return [self.emblems count];
        }else{
         //   NSLog(@"--- Title Count: %i ---", [self.titles count]);
            return [self.titles count];
            
        }
        
      //  NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
        
    }
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
    
    //TITLES
    if([self.title isEqualToString:@"Titles"]){
    
    title *title1 = [title alloc];
    
    
       
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Achievement"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Achievement"];
    }
    
    // is searching
    if (tableView == self.searchDisplayController.searchResultsTableView){
        title1 = [self.filteredListItems objectAtIndex:indexPath.row];
        cell.textLabel.text = title1.title;
         
    }
    else{
        
        title1 = [self.titles objectAtIndex:indexPath.row];
       
           }
    
    

        UIImageView *symbol = (UIImageView *)[cell viewWithTag:100];
        symbol.image = title1.image;
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
        nameLabel.text = title1.title;
        
  
    
    
    //NSLog(@"The achievement is %@",achievement1.titleImage);
    
    
   

        //cell.imageView.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2);
    //cell.imageView.image = achivo.thumbImage;
       
    
    //cell.textLabel.text = achivo.title;
    
    //cell.contentView.backgroundColor = [UIColor colorWithPatternImage:achivo.thumbImage];
   
    //cell.textLabel.backgroundColor = [ UIColor clearColor ];
    //cell.textLabel.textAlignment = UITextAlignmentCenter;
        
        
        return cell;
        
    }
    else
    //EMBLEMS    
    {
     //   NSLog(@"inside emblem cellforRow");
        //create my emblem
        emblem *emblem1 = [emblem alloc];
        emblem1 = [self.emblems objectAtIndex:indexPath.row];
        
     //   NSLog(@"--- Emblem Row selected %i >>>",indexPath.row);
        //create cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Achievement"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Achievement"];
        }

       
        
        //populate my cell with emblem data
        
        UIImageView *symbol = (UIImageView *)[cell viewWithTag:100];
        symbol.image = emblem1.image;   
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:101];
        if (emblem1.description.length > 13) {
            nameLabel.text =  [[emblem1.description substringToIndex:13] stringByAppendingString:@".."];
        }else   
        {
            nameLabel.text =  emblem1.description;

        }
        
        
   
     return cell;
    }
    
    
  //  NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
   

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  //  NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
    DetailViewController *detailController = segue.destinationViewController;
   
    
    
    //EMBLEMS no search
    if ([self.title isEqualToString:@"Emblems"]) {
       //  NSLog(@"--- Set Emblem Detail Row %i", self.tableViewEmblem.indexPathForSelectedRow.row);
        emblem* myEmblem = [self.emblems objectAtIndex:self.tableViewEmblem.indexPathForSelectedRow.row];
        detailController.detailItem = myEmblem;
       
    }else{
        //TITLES
    
    
        
        if (savedSearchTerm){   //set the detailViewController with the searched data cell 
            title* c = [self.filteredListItems objectAtIndex:_searchRowSelected];
            detailController.detailItem = c;
        }else{    //set the detailViewController with the original data cell
            
            
            title* c = [self.titles objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            detailController.detailItem = c;

        }
    }
    NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        self.searchRowSelected = indexPath.row;  //<-- the trick that worked
         if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.detailViewController.detailItem = [self.filteredListItems objectAtIndex:indexPath.row];
         }else{
        
        [self performSegueWithIdentifier:@"Detail" sender:self];
         }
    }else
    {    
        if ([self.title isEqualToString:@"Emblems"]) {
         self.detailViewController.detailItem = [self.emblems objectAtIndex:indexPath.row];
        // NSLog(@"<<< EMBLEM SELECTED %i >>>", indexPath.row);
        }else{
            self.detailViewController.detailItem = [self.titles objectAtIndex:indexPath.row];
          //  NSLog(@"<<< TITLE SELECTED %i >>>", indexPath.row);
        }
        
    }
    NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
}


- (void)filterContentForSearchText:(NSString*)searchString 
{
NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
	[self.filteredListItems removeAllObjects]; // Clear the filtered array.
    [self setSavedSearchTerm:searchString];
    
     if ([searchString length] !=0) 
    {
                
        for (title *ititle in [self titles])
		{
			if ([ititle.title rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound)
			{
				[self.filteredListItems addObject:ititle];
			}
		}

    
    }
    NSLog(@"filterContentForSearchText: Filtered Items Count=%i",[self.filteredListItems count]);
    NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
    //self.filteredListItems = self.getAchivos;  
  }



- (BOOL)searchDisplayController:(UISearchDisplayController *)controller 
shouldReloadTableForSearchString:(NSString *)searchString
{
   [self filterContentForSearchText:searchString];
    
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller 
shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    
    return YES;
}



- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
	NSLog(@">>> Entering %s <<<", __PRETTY_FUNCTION__);
	
	[self setSavedSearchTerm:nil];
	
	NSLog(@"<<< Leaving %s >>>", __PRETTY_FUNCTION__);
}



@end
