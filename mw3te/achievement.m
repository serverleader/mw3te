//
//  achivement.m
//  mw3te
//
//  Created by Carlos Rivero on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "achievement.h"

@implementation achievement 

#define kTitleKey       @"TitleName"
#define kTitleDesKey      @"TitleDescription"
#define kTitleImageKey      @"TitleDescription"

@synthesize titleName = _title;
@synthesize titlePage = _page;
@synthesize titleDescription = _titleDescription;
@synthesize titleType = _type;
@synthesize titleImage = _thumbImage;
@synthesize fullImage = _fullImage;
@synthesize unlocked = _unlocked;
@synthesize emblemDescription = _emblemDescription;
@synthesize emblemImage = _emblemImage;
@synthesize emblemPage = _emblemPage;
@synthesize emblemType = _emblemType;

- (id)initWithName:(NSString*)titleName details:(NSString*)details page:(NSInteger)page type:(NSInteger)type thumbImage:(UIImage *)titleImage fullImage:(UIImage *)fullImage unlocked:(NSInteger)unlocked {   
    if ((self = [super init])) {
        self.titleName = titleName;
        self.titlePage = page;
        self.titleDescription = details;
        self.titleType = type;
        self.titleImage = titleImage;
        self.fullImage = fullImage;
    }
    return self;
}


- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_title forKey:kTitleKey];
    [aCoder encodeObject:_titleDescription forKey:kTitleDesKey];
    [aCoder encodeObject:_thumbImage forKey:kTitleImageKey]; 
    
}

- (id) initWithCoder:(NSCoder *)coder
{
    
    return [self initWithName:[coder decodeObjectForKey:kTitleKey] details:[coder decodeObjectForKey:kTitleDesKey] page:1 type:1 thumbImage:[coder decodeObjectForKey:kTitleImageKey]  fullImage:[coder decodeObjectForKey:kTitleImageKey] unlocked:1];
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    [self setValuesForKeysWithDictionary:dictionary];
    //NSLog(@"The achievement is %@",self.titleName);
    //NSLog(@"The achievement Image: %@",self.titleImage);
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"titleImage"] || [key isEqualToString:@"emblemImage"]) {
        UIImage *img = [UIImage imageNamed:value];   
        value = img;
    }
    
    [super setValue:value forKey:key];
    
}

@end
