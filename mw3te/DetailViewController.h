//
//  DetailViewController.h
//  mw3te
//
//  Created by Carlos Rivero on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>
{
    
    IBOutlet UITextView *detailDescriptionLabel;
    IBOutlet UIImageView *imagen;
    IBOutlet UILabel *titulo;
    id detailItem;
}

@property (retain, nonatomic) id detailItem;

@property (strong, nonatomic) IBOutlet UITextView *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (strong, nonatomic) IBOutlet UITextView *emblemDetailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *emblemImagen;
@property (weak, nonatomic) IBOutlet UITextView *emblemTitulo;
@end
