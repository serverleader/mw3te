//
//  emblems.m
//  mw3te
//
//  Created by Carlos Rivero on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "emblem.h"

@implementation emblem
@synthesize description = _description;
@synthesize page = _page;
@synthesize type = _type;
@synthesize image = _image;
@synthesize sectionx = _section;




- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    [self setValuesForKeysWithDictionary:dictionary];
    //NSLog(@"The emblem is %@",self.description);
    //NSLog(@"The emblem Image: %@",self.image);
    
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"image"]) {
        UIImage *img = [UIImage imageNamed:value];   
        value = img;
    }
    if ([key isEqualToString:@"section"]) {
        self.sectionx = value;
        return;
    }
    [super setValue:value forKey:key];
    
}





@end
