//
//  achivement.h
//  mw3te
//
//  Created by Carlos Rivero on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface achievement : NSObject <NSCoding>

@property (retain) NSString *titleName;
@property (strong) NSString *titleDescription;
@property (assign) NSInteger titlePage;
@property (assign) NSInteger emblemPage;
@property (assign) NSInteger emblemType;
@property (assign) NSInteger titleType;
@property (strong) UIImage *titleImage;
@property (strong) UIImage *emblemImage;
@property (strong) UIImage *fullImage;
@property (assign) NSInteger unlocked;
@property (retain) NSString *emblemDescription;

 

- (id)initWithName:(NSString*)title details:(NSString*)details page:(NSInteger)page type:(NSInteger)type thumbImage:(UIImage *)thumbImage fullImage:(UIImage *)fullImage unlocked:(NSInteger)unlocked;  


- (id) initWithCoder:(NSCoder *)coder;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
