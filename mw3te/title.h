//
//  title.h
//  mw3te
//
//  Created by Carlos Rivero on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface title : NSObject

@property (retain) NSString *title;
@property (retain) NSString *description;
@property (assign) NSString *page;
@property (assign) NSString *type;
@property (strong) UIImage *image;


- (id)initWithDictionary:(NSDictionary *)dictionary;

@end
