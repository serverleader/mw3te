//
//  MasterViewController.h
//  mw3te
//
//  Created by Carlos Rivero on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;
@class MDDetailViewController;

@interface MasterViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>{
    DetailViewController *detailViewController;   
}

@property (nonatomic,retain) NSMutableArray *filteredListItems;
@property (retain, nonatomic) IBOutlet DetailViewController *detailViewController;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITableView *tableViewEmblem;
@property (nonatomic, assign) int searchRowSelected;
@property (assign,nonatomic) NSString *savedSearchTerm;
// my list of achiements
@property (strong) NSMutableArray *titles;
@property (strong) NSMutableArray *emblems;
- (void)filterContentForSearchText:(NSString*)searchString;

@end
